# Markdown Share Firefox Extension

## Publishing

### [Signing your extension for self-distribution](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext/)

[Generate developer credentials](https://addons.mozilla.org/en-US/developers/addon/api/key/) and run:

```
npx web-ext sign --api-key=$AMO_JWT_ISSUER --api-secret=$AMO_JWT_SECRET
```

### Generate (Gitlab) Release

### Manually upload artifacts / [generic packages](https://docs.gitlab.com/ee/user/packages/generic_packages/index.html#publish-a-package-file)

```
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     --upload-file <file> \
     "https://gitlab.example.com/api/v4/projects/23491175/packages/generic/<package>/<version>/<file>"
```

These packages can be found [Gitlab Package Registry](https://gitlab.com/pedrodavid96/md-share-ff-ext/-/packages/23491175).

The respective asset links were manually added as "external assets" in the release generated from the UI.